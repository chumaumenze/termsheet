__all__ = ["Spreadsheet", "Cell"]

import datetime as dt

from collections import defaultdict

from termsheet.models import Index
from termsheet.formula import parse

NaN = "#NaN!"


class Cell(object):
    formatted_value: str

    def __init__(self, sheet):
        self.sheet = sheet
        self._raw = str()
        self.ref_cell = None
        self._formatter = lambda x: str(x)
        self.formatted_value = str()

    @property
    def is_nan(self):
        return self.formatted_value == NaN

    @property
    def raw(self):
        return self._raw

    @raw.setter
    def raw(self, v: str):
        self._raw = v
        self.format_cell()

    @property
    def format(self):
        return self._formatter

    @format.setter
    def format(self, f: callable):
        """Set the format function"""
        self._formatter = f
        if self.raw:
            self.format_cell()

    def format_cell(self):
        """Format a cell to given format type"""
        eval_value = (
            self.ref_cell.eval_value if self.is_ref
            else self.eval_value
        )
        try:
            self.formatted_value = self.format(eval_value)
        except:
            self.formatted_value = NaN

    def compute_formula(self, parsed_formula: list):
        # TODO Iterative/Recursive approach to calculate nested ops
        pass

    @property
    def is_ref(self):
        return self.ref_cell is not None and isinstance(self.ref_cell, Cell)

    @property
    def eval_value(self):
        """Evaluates the raw text that the user entered into the cell.

        Calculates `raw` that reference other cells like ``'=A2+A3'``

        Returns:
            str: the value gotten from the evaluated `raw`.
        """
        v = self.raw
        if self.raw and self.raw.startswith('='):
            raw_value = self.raw[1:]

            try:
                parsed_raw = parse(raw_value)
            except ValueError:
                pass
            else:
                if isinstance(parsed_raw, str):
                    # Refers to another cell
                    ref_index = Index.parse(parsed_raw)
                    ref_cell = self.sheet.get_cell(ref_index)
                    v = ref_cell.eval_value
                    self.ref_cell = ref_cell
                else:
                    # Computes a formula for the cell
                    # v = self.compute_formula(parsed_raw)
                    pass
        else:
            self.ref_cell = None
        return v


class Spreadsheet:
    """The spreadsheet engine."""

    def __init__(self, fd):
        self.fd = fd
        self.sheet = defaultdict(lambda **x: [Cell(self)])

    def get_cell(self, index):
        """Get the cell at the current index"""
        self.sheet.setdefault(index.row_label, [Cell(self)])
        current_row = self.sheet[index.row_label]
        row_size = len(current_row)

        # Extend row if cell does not exit
        if index.col >= row_size:
            ext_size = (index.col - row_size) + (len(current_row) // 2) + 1
            current_row.extend([Cell(self) for i in range(ext_size)])

        return current_row[index.col]

    def get_formatted(self, index):
        """Get the evaluated and formatted value at the given cell ref.

        Arguments:
            index (Index): the cell to evaluate

        Returns:
            str: the cell value, evaluated (if a formula) and formatted
            according to the format set with `set_format`.
        """
        cell = self.get_cell(index)
        return cell.formatted_value

    def get_raw(self, index):
        """Get the raw text that the user entered into the given cell.

        Arguments:
            index (Index): the cell to fetch

        Returns:
            str: the `raw` most recently set with `set`.
        """
        cell = self.get_cell(index)
        return cell.raw

    def set(self, index, raw):
        """Set the value at the given cell.

        Arguments:
            index (Index): the cell to update
            raw (str): the raw string, like ``'1'`` or ``'2018-01-01'`` or ``'=A2+A3'``
        """
        cell = self.get_cell(index)
        cell.raw = raw

    def copy(self, src, dest):
        """Copy the cell range `src` to `dest`.

        Arguments:
            src (Range): the range to copy
            dest (Index): the cell into which the upper-left of `src` should go
        """
        raise NotImplementedError(f"copy {src} {dest}")

    def sort(self, range, column, ascending):
        """Sort the given range by the given column.

        Arguments:
            range (Range): the range to sort.
            column (int): the integer index of the column to sort. Must lie
                between range.first.col and range.last.col (inclusive).
            ascending (bool): whether to sort in ascending (1, 2, 3) or
                descending (3, 2, 1) order.
        """
        raise NotImplementedError(f"sort {range} {column} {ascending}")

    def write_out(self):
        """Writes the current spreadsheet to file."""
        import logging
        logging.debug({
            k: [c.format_cell() or c.formatted_value for c in v]
            for k, v in self.sheet.items()
        })
        self.fd.seek(0, 0)
        rows = sorted(self.sheet.keys(), key=lambda x: int(x))
        logging.debug(f'Rows: {rows}')
        for k in rows:
            line = ','.join([
                c.format_cell() or c.formatted_value for c in self.sheet[k]
            ]) + '\n'
            # for cell in values:
            #     cell.format_cell()
            #     line += f'{cell.formatted_value},'
            # line = line.strip(',') + '\n'
            self.fd.write(line)

    def set_format(self, index, type, spec):
        """Set the format string for a given range.

        Arguments:
            index (Index): the cell to format
            type (str): the type of format--``'default'``, ``'number'`` or ``'date'``
            spec (str): the format string to use on the cell:

                - if `type` is ``'default'``, should be None
                - if `type` is ``'number'``, a string suitable for passing to
                  python's string % operator, e.g. ``'%.2f'``
                - if `type` is ``'date'``, a string suitable for passing to
                  `datetime.strftime`, e.g. ``'%Y-%m-%d'``
        """
        cell = self.get_cell(index)
        type_formatters = {
            'default': lambda x: x,
            'number': lambda x: spec % float(x),
            'date': lambda x: dt.datetime.fromisoformat(x).strftime(spec)
        }
        formatter = type_formatters[type]
        cell.format = formatter
