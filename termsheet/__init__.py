#!/usr/bin/env python3

import os
import argparse
import csv
import curses
import logging
import pathlib

from termsheet import engine, views, models


def read_csv(fd, sheet):
    reader = csv.reader(fd)
    for row, values in enumerate(reader):
        for col, value in enumerate(values):
            sheet.set(models.Index(row, col), value)


def setup_logging():
    """Set up Python's log infrastructure with some simple defaults:
    - append to 'termsheet.log' in the 'python' directory
    - include the date/time on every message
    - set loglevel to DEBUG
    """
    logfile = pathlib.Path(__file__).parent.parent / "termsheet.log"
    logging.basicConfig(
        format="[%(asctime)s]: %(message)s", filename=logfile, level=logging.DEBUG
    )


def run(file_name):
    setup_logging()
    logging.info("--------------------")
    logging.info("Termsheet starting")

    @curses.wrapper
    def start_app(stdscr):
        curses.raw()
        file_mode = 'r+' if os.path.exists(file_name) else 'w+'

        try:
            with open(file_name, file_mode) as fd:
                sheet = engine.Spreadsheet(fd)
                read_csv(fd, sheet)
                viewer = views.Viewer(sheet, stdscr)
                viewer.loop()
            logging.info("Exiting.")
        finally:
            curses.noraw()


def main():
    parser = argparse.ArgumentParser(
        prog='termsheet', description='A command-line spreadsheet application')

    parser.add_argument('filename', metavar='filename', help='Input CSV file')

    args = parser.parse_args()

    if args.filename:
        run(args.filename)


if __name__ == "__main__":
    main()
