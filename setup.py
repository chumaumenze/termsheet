from setuptools import setup, find_packages

with open('README.md') as readme:
    long_description = readme.readline()

setup(
    name='termsheet',
    version='0.1.0',
    url='https://gitlab.com/chumaumenze/termsheet',
    license='MIT',
    author='Chuma Umenze',
    author_email='me@chumaumenze.com',
    description='A command-line spreadsheet application',
    long_description=long_description, # __doc__,
    long_description_content_type="text/markdown",
    packages=find_packages(exclude=('examples',)),
    entry_points={
        'console_scripts': [
            'termsheet = termsheet:main'
        ]
    },
    zip_safe=True,
    # include_package_data=True,
    platforms='any',
    install_requires=[],
    classifiers=[
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Software Development :: Libraries :: Python Modules'
    ]
)
