## Termsheet

A command-line spreadsheet application.

### Setup

+ Clone this repository - `git clone https://gitlab.com/chumaumenze/termsheet.git`
+ To install, run `pip install .`
+ To open/create a new spreadsheet, run `termsheet path/to/file.csv`.

### Todo

+ [ ] Handle large file that can't fit in-memory
+ [x] Implement the save functionality
+ [ ] Implement the sort functionality
+ [ ] Implement the copy functionality
+ [ ] Process complex spreadsheet formulas
